import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/user/user.module';
import { AdminModule } from './modules/admin/admin.module';
import { TopicModule } from './modules/topic/topic.module';
import { QuizModule } from './modules/quiz/quiz.module';
import { UserquizModule } from './modules/userquiz/userquiz.module';
import { TeamModule } from './modules/team/team.module';
import { InvitationModule } from './modules/invitation/invitation.module';
import { EmailModule } from './modules/email/email.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

const getMongoConfig = async (configService: ConfigService): Promise<MongooseModuleOptions> => ({
    uri: configService.get<string>('MONGODB_URI'),
});

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true }),
        MongooseModule.forRootAsync({
            imports: undefined,
            useFactory: getMongoConfig,
            inject: [ConfigService],
        }),
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '..', 'docs'),
            serveRoot: '/api/documentation',
        }),
        AuthModule,
        UserModule,
        AdminModule,
        TopicModule,
        QuizModule,
        UserquizModule,
        TeamModule,
        InvitationModule,
        EmailModule,
    ],
})
export class AppModule {}
