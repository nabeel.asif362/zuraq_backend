import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as morgan from 'morgan';
import { ConfigService } from '@nestjs/config';
import { INestApplication, ValidationPipe } from '@nestjs/common';

async function configureApp(app: INestApplication): Promise<void> {
    const configService: ConfigService = app.get(ConfigService);
    const serverPort: string = configService.get<string>('SERVER_PORT');

    app.enableCors();
    app.setGlobalPrefix('/v1');
    app.use(morgan('dev'));
    app.useGlobalPipes(new ValidationPipe({ whitelist: true }));

    await app.listen(serverPort);
}

async function bootstrap() {
    const app: INestApplication = await NestFactory.create(AppModule, { bodyParser: true });
    await configureApp(app);
}

bootstrap();
