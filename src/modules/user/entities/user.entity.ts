import { Exclude, Expose } from 'class-transformer';

export class UserEntity {
    @Expose()
    firstName: string;

    @Expose()
    lastName: string;

    @Expose()
    email: string;

    @Exclude()
    password: string;

    @Expose()
    role: string;

    @Expose()
    isApproved: boolean;

    @Exclude()
    forgotPasswordToken: string;
}
