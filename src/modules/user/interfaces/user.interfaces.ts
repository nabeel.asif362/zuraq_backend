import { Types } from 'mongoose';

export interface IUser {
    _id: Types.ObjectId;
    firstName: string;
    lastName: string;
    email: string;
    bio: string;
    description: string;
    avatar: string;
    password: string;
    role: string;
    isApproved: boolean;
    forgotPasswordToken: string;
    createdAt: Date;
    updatedAt: Date;
}
