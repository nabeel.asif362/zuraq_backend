/**
 * @apiDefine UserSuccess
 * @apiSuccess {String} _id Unique identifier of the user.
 * @apiSuccess {String} name User's name.
 * @apiSuccess {String} email User's email address.
 * @apiSuccess {String="USER","ADMIN","CREATOR"} role Role of the user in the application.
 * @apiSuccess {String|null} bio User's biography (optional).
 * @apiSuccess {String|null} description Additional description about the user (optional).
 * @apiSuccess {String|null} avatar URL to the user's avatar image (optional).
 * @apiSuccess {String} createdAt Timestamp when the user was created.
 * @apiSuccess {String} updatedAt Timestamp when the user was last updated.
 */

/**
 * @apiDefine UnauthorizedError
 * @apiError (401 Unauthorized) {String} message Error message indicating unauthorized access.
 * @apiError (401 Unauthorized) {Number} statusCode HTTP status code 401.
 *
 * @apiErrorExample {json} 401 Unauthorized Response:
 *    HTTP/1.1 401 Unauthorized
 *    {
 *      "message": "Unauthorized",
 *      "statusCode": 401
 *    }
 */

/**
 * @api {get} /user/profile Get My Profile
 * @apiName GetMyProfile
 * @apiGroup User
 * @apiHeader {String} Authorization Bearer token required for authentication. Format: "Bearer <token>"
 * @apiUse UserSuccess
 * @apiUse UnauthorizedError
 */
