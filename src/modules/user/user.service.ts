import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './schemas/user.schema';
import { Model } from 'mongoose';
import { CloudinaryService } from '../../utils/cloudinary.utils';
import { UpdateProfileBodyDto } from './dto/index.dto';

@Injectable()
export class UserService {
    constructor(
        @InjectModel(User.name) private readonly userModel: Model<User>,
        private readonly cloudinaryService: CloudinaryService,
    ) {}

    async findProfile(user: User): Promise<User> {
        return await this.userModel
            .findById(user._id)
            .select(['_id', 'name', 'email', 'role', 'bio', 'description', 'avatar', 'createdAt', 'updatedAt'])
            .exec();
    }

    async updateProfile(avatar: Express.Multer.File, body: UpdateProfileBodyDto, user: User): Promise<void> {
        const avatarUrl = avatar
            ? await this.cloudinaryService
                  .uploadFile(avatar.buffer)
                  .then((response) => response.fileUrl)
                  .catch((error) => {
                      console.error('Cloudinary Service Error: \n', error);
                      return null;
                  })
            : '';

        await this.userModel.findByIdAndUpdate(user._id, { ...body, avatar: avatarUrl });
    }

    async removeUser(user: User): Promise<void> {
        await this.userModel.findByIdAndDelete(user._id);
    }
}
