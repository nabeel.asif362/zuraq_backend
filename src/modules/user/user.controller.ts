import { Body, Controller, Delete, Get, Patch, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { UserService } from './user.service';
import { AuthGuard } from '../../guards/auth.guard';
import { User } from './schemas/user.schema';
import { UserDecorator } from '../../decorators/user.decorator';
import { FileInterceptor } from '@nestjs/platform-express';
import { UpdateProfileBodyDto } from './dto/index.dto';

@Controller('user')
@UseGuards(AuthGuard)
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Get('profile')
    findProfile(@UserDecorator() user: User): Promise<User> {
        return this.userService.findProfile(user);
    }

    @Patch('profile')
    @UseInterceptors(FileInterceptor('avatar'))
    updateProfile(
        @UploadedFile() avatar: Express.Multer.File,
        @Body() body: UpdateProfileBodyDto,
        @UserDecorator() user: User,
    ): Promise<void> {
        return this.userService.updateProfile(avatar, body, user);
    }

    @Delete()
    removeUser(@UserDecorator() user: User): Promise<void> {
        return this.userService.removeUser(user);
    }
}
