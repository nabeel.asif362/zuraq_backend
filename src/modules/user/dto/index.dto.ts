import { IsOptional, IsString } from 'class-validator';

export class UpdateProfileBodyDto {
    @IsOptional()
    @IsString()
    name: string;

    @IsOptional()
    @IsString()
    bio: string;

    @IsOptional()
    @IsString()
    description: string;
}
