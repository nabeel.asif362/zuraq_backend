import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export enum UserRole {
    USER = 'USER',
    CREATOR = 'CREATOR',
    ADMIN = 'ADMIN',
}

@Schema({ timestamps: true })
export class User extends Document {
    @Prop({ required: true })
    name: string;

    @Prop({ required: true, unique: true })
    email: string;

    @Prop({ required: true })
    password: string;

    @Prop({ default: UserRole.USER })
    role: string;

    @Prop({ default: false })
    isApproved: boolean;

    @Prop({ default: null })
    bio: string;

    @Prop({ default: null })
    description: string;

    @Prop({ default: null })
    avatar: string;

    @Prop({ default: null })
    forgotPasswordToken: string;

    @Prop({ default: null })
    provider: string;

    @Prop({ default: null })
    providerId: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
