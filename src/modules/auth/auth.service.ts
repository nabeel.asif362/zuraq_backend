import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { CreatorSignupDto, ForgotPasswordDto, LoginDto, ResetPasswordDto, UserSignupDto } from './dto/index.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserRole } from '../user/schemas/user.schema';
import { AuthGuard } from '../../guards/auth.guard';
import { EmailService } from '../email/email.service';
import { randomBytes } from 'node:crypto';

@Injectable()
export class AuthService {
    constructor(
        @InjectModel(User.name) private readonly userModel: Model<User>,
        private readonly authGuard: AuthGuard,
        private readonly emailService: EmailService,
    ) {}

    async userSignup(body: UserSignupDto): Promise<void> {
        if (body.password !== body.confirmPassword) throw new BadRequestException('Passwords do not match.');

        const isUser: User = (await this.userModel.findOne({ email: body.email })) as User;
        if (isUser) throw new BadRequestException('Email already in use.');

        const saltOrRounds: number = 10;
        const hashedPassword: string = await bcrypt.hash(body.password, saltOrRounds);

        await this.userModel.create({
            name: body.name,
            email: body.email,
            password: hashedPassword,
            role: UserRole.USER,
            isApproved: true,
        });

        await this.emailService.welcomeEmail(body.name, body.email);
    }

    async creatorSignup(body: CreatorSignupDto) {
        if (body.password !== body.confirmPassword) throw new BadRequestException('Passwords do not match.');

        const isEmailUsed: User = (await this.userModel.findOne({ email: body.email })) as User;
        if (isEmailUsed) throw new BadRequestException('Email already in use.');

        const saltOrRounds: number = 10;
        const hashedPassword: string = await bcrypt.hash(body.password, saltOrRounds);

        await this.userModel.create({
            name: body.name,
            email: body.email,
            password: hashedPassword,
            role: UserRole.CREATOR,
            isApproved: false,
        });

        await this.emailService.welcomeEmail(body.name, body.email);
    }

    async login(body: LoginDto) {
        const isUser: User = (await this.userModel.findOne({ email: body.email })) as User;
        if (!isUser) throw new NotFoundException('Email does not exist.');

        const isValidPassword: boolean = await bcrypt.compare(body.password, isUser.password);
        if (!isValidPassword) throw new BadRequestException('Invalid Password.');

        if (isUser.role === UserRole.CREATOR && !isUser.isApproved) {
            throw new BadRequestException('You need to be approved by admin before logging in');
        }

        const authToken = this.authGuard.generateToken(isUser._id as string);

        return { authToken, role: isUser.role };
    }

    async forgotPassword(body: ForgotPasswordDto) {
        const isUser: User | null = await this.userModel.findOne({ email: body.email }).exec();

        if (!isUser) {
            throw new BadRequestException('User does not exist.');
        }

        const forgotPasswordToken = randomBytes(16).toString('hex');

        await this.emailService.forgotPasswordEmail(body.email, isUser.name, forgotPasswordToken);

        await this.userModel.updateOne({ email: body.email }, { forgotPasswordToken }).exec();
    }

    async resetPassword(body: ResetPasswordDto) {
        if (body.password !== body.confirmPassword) {
            throw new BadRequestException('Passwords do not match.');
        }

        const user: User | null = await this.userModel
            .findOne({ forgotPasswordToken: body.forgotPasswordToken })
            .exec();

        if (!user) {
            throw new BadRequestException('Invalid or expired password token.');
        }

        const saltOrRounds = 10;
        const hashedPassword = await bcrypt.hash(body.password, saltOrRounds);

        user.password = hashedPassword;
        user.forgotPasswordToken = null;
        await user.save();

        await this.emailService.resetPasswordConfirmationEmail(user.email, user.name);
    }

    async validateOAuthUser(provider: string, providerId: string, name: string, email: string): Promise<User> {
        let user: User | null = await this.userModel.findOne({ provider, providerId }).exec();

        if (!user) {
            user = await this.userModel.findOne({ email });

            if (user) {
                user.provider = provider;
                user.providerId = providerId;
                await user.save();
            } else {
                const saltOrRounds: number = 10;
                const hashedPassword: string = await bcrypt.hash('googleAuthPassword', saltOrRounds);

                user = await this.userModel.create({
                    name,
                    email,
                    password: hashedPassword,
                    provider,
                    providerId,
                    role: UserRole.USER,
                    isApproved: true,
                });

                await this.emailService.welcomeEmail(name, email);
            }
        }

        return user;
    }

    generateOAuthToken(user: User) {
        const authToken = this.authGuard.generateToken(user._id as string);
        return { authToken, role: user.role };
    }
}
