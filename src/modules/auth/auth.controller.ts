import { Body, Controller, Get, Post, UseGuards, Req, Res, Query, Patch } from '@nestjs/common';
import { AuthService } from './auth.service';
import {
    LoginDto,
    UserSignupDto,
    ResetPasswordDto,
    CreatorSignupDto,
    ForgotPasswordDto,
    GoogleAuthRedirectQuery,
} from './dto/index.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('signup/user')
    userSignup(@Body() body: UserSignupDto): Promise<void> {
        return this.authService.userSignup(body);
    }

    @Post('signup/creator')
    creatorSignup(@Body() body: CreatorSignupDto): Promise<void> {
        return this.authService.creatorSignup(body);
    }

    @Post('login')
    login(@Body() body: LoginDto) {
        return this.authService.login(body);
    }

    @Post('forgot-password')
    forgotPassword(@Body() body: ForgotPasswordDto) {
        return this.authService.forgotPassword(body);
    }

    @Patch('reset-password')
    resetPassword(@Body() body: ResetPasswordDto) {
        return this.authService.resetPassword(body);
    }

    @Get('google')
    @UseGuards(AuthGuard('google'))
    async googleAuth(): Promise<void> {}

    @Get('google/callback')
    @UseGuards(AuthGuard('google'))
    googleAuthRedirect(@Req() req: any, @Res() res: any, @Query() query: GoogleAuthRedirectQuery) {
        if (query?.error === 'access_denied') {
            res.redirect(`${process.env.FRONTED_BASE_URL}/user/login`);
        }

        try {
            const user = req?.user;
            const { authToken, role } = this.authService.generateOAuthToken(user);
            res.redirect(`${process.env.FRONTED_BASE_URL}/auth/google/callback?token=${authToken}&role=${role}`);
        } catch (e) {
            console.error(e);
            res.redirect(`${process.env.FRONTED_BASE_URL}/auth/google/callback`);
        }
    }
}