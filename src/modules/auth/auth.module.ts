import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../user/schemas/user.schema';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '../../guards/auth.guard';
import { EmailService } from '../email/email.service';
import { GoogleStrategy } from './auth.google.strategy';

@Module({
    imports: [MongooseModule.forFeature([{ name: User.name, schema: UserSchema }])],
    controllers: [AuthController],
    providers: [AuthService, JwtService, AuthGuard, EmailService, GoogleStrategy],
})
export class AuthModule {}
