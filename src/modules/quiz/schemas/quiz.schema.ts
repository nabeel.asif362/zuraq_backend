import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Topic } from '../../topic/schemas/topic.schema';

@Schema()
export class Option extends Document {
    @Prop({ required: true })
    optionText: string;

    @Prop({ required: true })
    isCorrect: boolean;
}

const OptionSchema = SchemaFactory.createForClass(Option);

@Schema()
export class Question extends Document {
    @Prop({ required: true })
    questionText: string;

    @Prop({ type: [OptionSchema], _id: true })
    options: Option[];

    @Prop()
    correctOptionDescription: string;
}

const QuestionSchema = SchemaFactory.createForClass(Question);

@Schema({ timestamps: true })
export class Quiz extends Document {
    @Prop({ type: Types.ObjectId, ref: 'Topic', required: true })
    topic: Topic;

    @Prop({ required: true })
    title: string;

    @Prop({ default: true })
    isPublic: boolean;

    @Prop({ type: Types.ObjectId, ref: 'User', required: true })
    createdBy: Types.ObjectId;

    @Prop({ enum: ['daily', 'weekly'], required: true })
    frequency: 'daily' | 'weekly';

    @Prop({ type: [QuestionSchema], _id: true })
    questions: Question[];
}

export const QuizSchema = SchemaFactory.createForClass(Quiz);
