import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

@Schema()
export class Invitation extends Document {
    @Prop({ type: Types.ObjectId, ref: 'User', required: true })
    sender: Types.ObjectId;

    @Prop({ required: true })
    recipientEmail: string;

    @Prop({ type: Types.ObjectId, ref: 'Team' })
    team?: Types.ObjectId;

    @Prop({ type: Types.ObjectId, ref: 'Quiz' })
    quiz?: Types.ObjectId;

    @Prop({ enum: ['pending', 'accepted', 'declined'], default: 'pending' })
    status: string;

    @Prop({ default: Date.now })
    sentAt: Date;
}

export const InvitationSchema = SchemaFactory.createForClass(Invitation);
