import { Module } from '@nestjs/common';
import { QuizService } from './quiz.service';
import { QuizController } from './quiz.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../user/schemas/user.schema';
import { JwtService } from '@nestjs/jwt';
import { Quiz, QuizSchema } from './schemas/quiz.schema';
import { UserQuiz, UserQuizSchema } from '../userquiz/schemas/userquiz.schema';
import { Topic, TopicSchema } from '../topic/schemas/topic.schema';
import { EmailService } from '../email/email.service';
import { MailerService } from '@nestjs-modules/mailer';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: User.name, schema: UserSchema },
            {
                name: Quiz.name,
                schema: QuizSchema,
            },
            {
                name: UserQuiz.name,
                schema: UserQuizSchema,
            },
            {
                name: Topic.name,
                schema: TopicSchema,
            },
        ]),
    ],
    controllers: [QuizController],
    providers: [QuizService, JwtService, EmailService],
})
export class QuizModule {}
