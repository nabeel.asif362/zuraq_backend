import { Types } from 'mongoose';

export interface IOption {
    optionText: string;
    isCorrect: boolean;
}

export interface IQuestion {
    _id: Types.ObjectId;
    questionText: string;
    options: IOption[];
}

export interface IQuiz {
    topic: Types.ObjectId;
    title: string;
    isPublic: boolean;
    createdBy: Types.ObjectId;
    frequency: 'daily' | 'weekly';
    questions: IQuestion[];
}
