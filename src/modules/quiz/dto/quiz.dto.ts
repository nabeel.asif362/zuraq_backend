import { Types } from 'mongoose';
import { Type } from 'class-transformer';
import { IsArray, IsBoolean, IsMongoId, IsNotEmpty, IsOptional, IsString, ValidateNested } from 'class-validator';

export class CreateOptionDto {
    @IsNotEmpty()
    @IsString()
    optionText: string;

    @IsNotEmpty()
    @IsBoolean()
    isCorrect: boolean;
}

export class CreateQuestionDto {
    @IsNotEmpty()
    @IsString()
    questionText: string;

    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => CreateOptionDto)
    options: CreateOptionDto[];

    @IsNotEmpty()
    @IsString()
    correctOptionDescription: string;
}

export class CreateQuizDto {
    @IsNotEmpty()
    @IsMongoId()
    topic: Types.ObjectId;

    @IsNotEmpty()
    @IsString()
    title: string;

    @IsNotEmpty()
    @IsBoolean()
    isPublic: boolean;

    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => CreateQuestionDto)
    questions: CreateQuestionDto[];
}

export class UpdateQuizParamsDto {
    @IsNotEmpty()
    @IsMongoId()
    id: string;
}

export class DeleteQuizParamsDto extends UpdateQuizParamsDto {}

export class FindQuizByIdParamsDto extends UpdateQuizParamsDto {}

export class FindAllQuizByTopicIdParamDto extends UpdateQuizParamsDto {}

export class ShareQuizWithTeamParamDto extends UpdateQuizParamsDto {}

export class ShareQuizWithTeamBodyDto {
    @IsArray()
    @IsNotEmpty()
    members: [{ name: string; email: string }];
}

export class FindAllQuizQueryDto {
    @IsOptional()
    @IsString()
    isPublic: string;
}
