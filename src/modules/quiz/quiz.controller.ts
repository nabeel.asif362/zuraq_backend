import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { QuizService } from './quiz.service';
import {
    CreateQuizDto,
    DeleteQuizParamsDto,
    FindAllQuizByTopicIdParamDto,
    FindAllQuizQueryDto,
    FindQuizByIdParamsDto,
    ShareQuizWithTeamBodyDto,
    ShareQuizWithTeamParamDto,
    UpdateQuizParamsDto,
} from './dto/quiz.dto';
import { AuthGuard } from '../../guards/auth.guard';
import { UserDecorator } from '../../decorators/user.decorator';
import { IUser } from '../user/interfaces/user.interfaces';
import { Quiz } from './schemas/quiz.schema';
import { User } from '../user/schemas/user.schema';

@Controller('quiz')
export class QuizController {
    constructor(private readonly quizService: QuizService) {}

    @UseGuards(AuthGuard)
    @Post()
    create(@Body() body: CreateQuizDto, @UserDecorator() user: IUser): Promise<void> {
        return this.quizService.create(body, user);
    }

    @UseGuards(AuthGuard)
    @Get()
    findAll(@Query() query: FindAllQuizQueryDto, @UserDecorator() user: IUser): Promise<any> {
        return this.quizService.findAll(query, user);
    }

    @Get('open')
    findAllOpenQuizzes(@Query() query: FindAllQuizQueryDto): Promise<any> {
        return this.quizService.findAllOpenQuizzes(query);
    }

    @UseGuards(AuthGuard)
    @Get('user')
    findMyQuizzesAndScores(@UserDecorator() user: IUser): Promise<any> {
        return this.quizService.findMyQuizzesAndScores(user);
    }

    @Get(':id')
    findById(@Param() params: FindQuizByIdParamsDto): Promise<Quiz> {
        return this.quizService.findById(params);
    }

    @UseGuards(AuthGuard)
    @Patch(':id')
    updateOne(@Param() params: UpdateQuizParamsDto, @Body() body: CreateQuizDto): Promise<void> {
        return this.quizService.updateOne(params, body);
    }

    @UseGuards(AuthGuard)
    @Delete(':id')
    deleteOne(@Param() params: DeleteQuizParamsDto): Promise<void> {
        return this.quizService.deleteOne(params);
    }

    @UseGuards(AuthGuard)
    @Post(':id/team/share')
    shareQuizWithTeam(
        @Param() params: ShareQuizWithTeamParamDto,
        @Body() body: ShareQuizWithTeamBodyDto,
        @UserDecorator() user: User,
    ): Promise<void> {
        return this.quizService.shareQuizWithTeam(params, body, user);
    }

    @Get('topic/:id')
    findAllQuizByTopicId(@Param() param: FindAllQuizByTopicIdParamDto): Promise<Quiz[]> {
        return this.quizService.findAllQuizByTopicId(param);
    }
}
