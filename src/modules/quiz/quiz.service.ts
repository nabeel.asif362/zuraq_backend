import { BadRequestException, Injectable } from '@nestjs/common';
import {
    CreateQuizDto,
    DeleteQuizParamsDto,
    FindAllQuizByTopicIdParamDto,
    FindAllQuizQueryDto,
    FindQuizByIdParamsDto,
    ShareQuizWithTeamBodyDto,
    UpdateQuizParamsDto,
} from './dto/quiz.dto';
import { IUser } from '../user/interfaces/user.interfaces';
import { InjectModel } from '@nestjs/mongoose';
import { Quiz } from './schemas/quiz.schema';
import { Model } from 'mongoose';
import { User, UserRole } from '../user/schemas/user.schema';
import { UserQuiz } from '../userquiz/schemas/userquiz.schema';
import { Topic } from '../topic/schemas/topic.schema';
import { EmailService } from '../email/email.service';

@Injectable()
export class QuizService {
    constructor(
        @InjectModel(Quiz.name) private readonly quizModel: Model<Quiz>,
        @InjectModel(UserQuiz.name) private readonly userQuizModel: Model<UserQuiz>,
        @InjectModel(Topic.name) private readonly topicModel: Model<Topic>,
        private readonly emailService: EmailService,
    ) {}

    async create(body: CreateQuizDto, user: IUser): Promise<void> {
        if (user.role === UserRole.USER) {
            throw new BadRequestException(`You don't have the permission to create a quiz.`);
        }

        await this.quizModel.create({ ...body, frequency: 'daily', createdBy: user._id });
    }

    async findAll(query: FindAllQuizQueryDto, user: IUser): Promise<any> {
        const isPublic: boolean = query.isPublic ? JSON.parse(query.isPublic) : false;

        const quizzes: any = await this.quizModel
            .find({ isPublic })
            .populate('topic', '-createdAt -updatedAt -__v')
            .select('-__v -updatedAt -createdBy')
            .exec();

        const userAttemptedQuizzes: any = await this.userQuizModel.find({ user: user._id }).exec();

        const finalList: any = [];

        for (const quiz of quizzes) {
            const hasParticipatedInQuiz = userAttemptedQuizzes.some((uaq) => {
                return uaq.quiz === quiz._id.toString();
            });
            hasParticipatedInQuiz
                ? finalList.push({ ...quiz.toObject(), hasParticipated: true })
                : finalList.push({
                      ...quiz.toObject(),
                      hasParticipated: false,
                  });
        }

        return finalList;
    }

    async findAllOpenQuizzes(query: FindAllQuizQueryDto): Promise<Quiz[]> {
        const isPublic: boolean = query.isPublic ? JSON.parse(query.isPublic) : false;

        return await this.quizModel
            .find({ isPublic })
            .populate('topic', '-createdAt -updatedAt -__v')
            .select('-__v -updatedAt -createdBy')
            .exec();
    }

    async findById(params: FindQuizByIdParamsDto): Promise<Quiz> {
        const quiz: Quiz = await this.quizModel
            .findById(params.id)
            .populate('topic', '-createdAt -updatedAt -__v')
            .select('-__v -createdAt -updatedAt -createdBy')
            .exec();

        return quiz || ({} as Quiz);
    }

    async updateOne(params: UpdateQuizParamsDto, body: Partial<CreateQuizDto>): Promise<void> {
        await this.quizModel.updateOne({ _id: params.id }, body);
    }

    async deleteOne(params: DeleteQuizParamsDto): Promise<void> {
        await this.quizModel.findByIdAndDelete(params.id);
    }

    async shareQuizWithTeam(params: DeleteQuizParamsDto, body: ShareQuizWithTeamBodyDto, user: User): Promise<void> {
        const quiz: Quiz | any = await this.quizModel.findById(params.id).populate('topic').select('topic').exec();

        if (!quiz) {
            throw new BadRequestException('Invalid Quiz Id');
        }

        const quizInvitationPromiseList = body.members.map((member) =>
            this.emailService.quizInvitationEmail(params.id, member.email, member.name, user.name, quiz.topic.name),
        );

        await Promise.all(quizInvitationPromiseList);
    }

    async findAllQuizByTopicId(param: FindAllQuizByTopicIdParamDto): Promise<Quiz[]> {
        return await this.quizModel
            .find({ topic: param.id, isPublic: true })
            .populate('topic', '-createdAt -updatedAt -__v')
            .select('-__v -createdAt -updatedAt -createdBy')
            .exec();
    }

    async findMyQuizzesAndScores(user: IUser): Promise<any> {
        const myQuizzes: Quiz[] = await this.quizModel
            .find({ createdBy: user._id })
            .populate('topic', '-createdAt -updatedAt -__v')
            .select('-__v -createdAt -updatedAt -createdBy -questions.options.isCorrect')
            .exec();

        const userQuizzes: UserQuiz[] = await this.userQuizModel.find({ user: user._id }).exec();
        const quizIds = userQuizzes.map((uq) => uq.quiz);
        const quizzes: Quiz[] = await this.quizModel.find({ _id: { $in: quizIds } }).exec();

        const topicIds = quizzes.map((quiz) => quiz.topic);
        const topics: Topic[] = await this.topicModel.find({ _id: { $in: topicIds } }).exec();

        const quizToTopicMap = quizzes.reduce((acc, quiz) => {
            acc[quiz._id as any] = quiz.topic;
            return acc;
        }, {});

        const topicMap = topics.reduce((acc, topic) => {
            acc[topic._id as any] = topic.name;
            return acc;
        }, {});

        const scoreByTopic = userQuizzes.reduce((acc, userQuiz) => {
            const quizId = userQuiz.quiz;
            const topicId = quizToTopicMap[quizId as any];

            if (topicId) {
                if (!acc[topicId]) {
                    acc[topicId] = {
                        topicName: topicMap[topicId] || 'Unknown Topic',
                        totalScore: 0,
                    };
                }
                acc[topicId].totalScore += userQuiz.score;
            }

            return acc;
        }, {});

        const scores = Object.keys(scoreByTopic).map((topicId) => ({
            topicId,
            topicName: scoreByTopic[topicId].topicName,
            totalScore: scoreByTopic[topicId].totalScore,
        }));

        return {
            myQuizzes,
            scores,
        };
    }
}
