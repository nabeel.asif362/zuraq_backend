import { IsMongoId, IsNotEmpty, IsString, IsUrl } from 'class-validator';

export class ApproveCreatorDto {
    @IsNotEmpty()
    @IsMongoId()
    _id: string;
}

export class DisapproveCreatorDto extends ApproveCreatorDto {}

export class CreateBlogDto {
    @IsNotEmpty()
    @IsString()
    title: string;

    @IsNotEmpty()
    @IsUrl()
    url: string;
}

export class UpdateBlogParamDto extends ApproveCreatorDto {}

export class DeleteBlogParamDto extends ApproveCreatorDto {}
