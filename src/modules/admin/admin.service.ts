import { BadRequestException, Injectable } from '@nestjs/common';
import {
    ApproveCreatorDto,
    CreateBlogDto,
    DeleteBlogParamDto,
    DisapproveCreatorDto,
    UpdateBlogParamDto,
} from './dto/index.dto';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserRole } from '../user/schemas/user.schema';
import { Model } from 'mongoose';
import { plainToInstance } from 'class-transformer';
import { UserEntity } from '../user/entities/user.entity';
import { Quiz } from '../quiz/schemas/quiz.schema';
import { Topic } from '../topic/schemas/topic.schema';
import { UserQuiz } from '../userquiz/schemas/userquiz.schema';
import { Blog } from './schemas/blog.schema';

@Injectable()
export class AdminService {
    constructor(
        @InjectModel(User.name) private readonly userModel: Model<User>,
        @InjectModel(Quiz.name) private readonly quizModel: Model<Quiz>,
        @InjectModel(Topic.name) private readonly topicModel: Model<Topic>,
        @InjectModel(UserQuiz.name) private readonly userQuizModel: Model<UserQuiz>,
        @InjectModel(Blog.name) private readonly blogModel: Model<Blog>,
    ) {}

    async findAllCreators(): Promise<any> {
        const creators: User[] = (await this.userModel.find({ role: UserRole.CREATOR })) as User[];
        return plainToInstance(
            UserEntity,
            creators.map((creator) => creator.toObject()),
        );
    }

    async approveCreator(params: ApproveCreatorDto) {
        const isCreator: User = (await this.userModel.findById(params._id)) as User;
        if (!isCreator) throw new BadRequestException('Invalid Creator Id.');
        if (isCreator.isApproved) throw new BadRequestException('This creator is already approved.');

        await this.userModel.findByIdAndUpdate(params._id, { isApproved: true });
    }

    async disapproveCreator(params: DisapproveCreatorDto) {
        const isCreator: User = (await this.userModel.findById(params._id)) as User;
        if (!isCreator) throw new BadRequestException('Invalid Creator Id.');
        await this.userModel.findByIdAndDelete(params._id);
    }

    async findQuizzesByTopics(): Promise<any> {
        const quizzes: any = await this.quizModel.find();
        const topics: any = await this.topicModel.find();

        const topicMap = topics.reduce((acc, topic) => {
            acc[topic._id] = topic.name;
            return acc;
        }, {});

        const categorizedQuizzes = quizzes.reduce((acc, quiz) => {
            const topicId = quiz.topic.toString();
            const topicName = topicMap[topicId] || 'Unknown Topic';

            if (!acc[topicId]) {
                acc[topicId] = {
                    topicId: topicId,
                    topicName: topicName,
                    quizzes: [],
                };
            }

            acc[topicId].quizzes.push(quiz);

            return acc;
        }, {});

        return Object.values(categorizedQuizzes);
    }

    async findAllUsers(): Promise<any> {
        return await this.userModel.find().select('-password -teams -__v').exec();
    }

    async findLeaderboardScores(): Promise<any> {
        return this.userQuizModel
            .aggregate([
                {
                    $group: {
                        _id: '$user',
                        totalScore: { $sum: '$score' },
                    },
                },
                {
                    $sort: { totalScore: -1 },
                },
                {
                    $limit: 10,
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'userDetails',
                    },
                },
                {
                    $project: {
                        _id: 0,
                        totalScore: 1,
                        'userDetails.email': 1,
                        'userDetails.name': 1,
                    },
                },
                {
                    $unwind: '$userDetails',
                },
            ])
            .exec();
    }

    async addBlog(body: CreateBlogDto): Promise<any> {
        try {
            await this.blogModel.create(body);
        } catch (e) {
            if (e?.code === 11000) {
                throw new BadRequestException('Url already exists.');
            }
        }
    }

    async findAllBlogs(): Promise<any> {
        return await this.blogModel.find().exec();
    }

    async updateBlog(param: UpdateBlogParamDto, body: Partial<CreateBlogDto>): Promise<any> {
        try {
            return await this.blogModel.findByIdAndUpdate(param._id, body).exec();
        } catch (e) {
            if (e?.code === 11000) {
                throw new BadRequestException('Url already exists.');
            }
        }
    }

    async deleteBlog(param: DeleteBlogParamDto): Promise<any> {
        return await this.blogModel.findByIdAndDelete(param._id).exec();
    }
}
