import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { AdminService } from './admin.service';
import {
    ApproveCreatorDto,
    CreateBlogDto,
    DeleteBlogParamDto,
    DisapproveCreatorDto,
    UpdateBlogParamDto,
} from './dto/index.dto';

@Controller('admin')
export class AdminController {
    constructor(private readonly adminService: AdminService) {}

    @Get('creator')
    findAllCreators() {
        return this.adminService.findAllCreators();
    }

    @Patch('creator/approve/:_id')
    approveCreator(@Param() params: ApproveCreatorDto) {
        return this.adminService.approveCreator(params);
    }

    @Patch('creator/disapprove/:_id')
    disapproveCreator(@Param() params: DisapproveCreatorDto) {
        return this.adminService.disapproveCreator(params);
    }

    @Get('quiz-by-topic')
    findQuizzesByTopics() {
        return this.adminService.findQuizzesByTopics();
    }

    @Get('users')
    findAllUsers() {
        return this.adminService.findAllUsers();
    }

    @Get('leaderboard')
    findLeaderboardScores() {
        return this.adminService.findLeaderboardScores();
    }

    // --------- Blogs Management --------- //
    @Post('blog')
    addBlog(@Body() body: CreateBlogDto) {
        return this.adminService.addBlog(body);
    }

    @Get('blog')
    findAllBlogs() {
        return this.adminService.findAllBlogs();
    }

    @Patch('blog/:_id')
    updateBlog(@Param() param: UpdateBlogParamDto, @Body() body: Partial<CreateBlogDto>) {
        return this.adminService.updateBlog(param, body);
    }

    @Delete('blog/:_id')
    deleteBlog(@Param() param: DeleteBlogParamDto) {
        return this.adminService.deleteBlog(param);
    }
}
