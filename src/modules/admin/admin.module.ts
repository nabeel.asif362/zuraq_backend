import { Module } from '@nestjs/common';
import { AdminService } from './admin.service';
import { AdminController } from './admin.controller';
import { AuthGuard } from '../../guards/auth.guard';
import { JwtService } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../user/schemas/user.schema';
import { Quiz, QuizSchema } from '../quiz/schemas/quiz.schema';
import { Topic, TopicSchema } from '../topic/schemas/topic.schema';
import { UserQuiz, UserQuizSchema } from '../userquiz/schemas/userquiz.schema';
import { Blog, BlogSchema } from './schemas/blog.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: User.name, schema: UserSchema },
            {
                name: Quiz.name,
                schema: QuizSchema,
            },
            {
                name: Topic.name,
                schema: TopicSchema,
            },
            {
                name: UserQuiz.name,
                schema: UserQuizSchema,
            },
            {
                name: Blog.name,
                schema: BlogSchema,
            },
        ]),
    ],
    controllers: [AdminController],
    providers: [AdminService, JwtService, AuthGuard],
})
export class AdminModule {}
