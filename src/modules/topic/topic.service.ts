import { Injectable } from '@nestjs/common';
import { CreateTopicDto, DeleteTopicDto } from './dto/index.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Topic } from './schemas/topic.schema';
import { Model } from 'mongoose';

@Injectable()
export class TopicService {
    constructor(@InjectModel(Topic.name) private readonly topicModel: Model<Topic>) {}

    async create(body: CreateTopicDto) {
        await this.topicModel.create(body);
    }

    findAll() {
        return this.topicModel.find().select('-__v -createdAt -updatedAt');
    }

    async remove(params: DeleteTopicDto) {
        await this.topicModel.deleteOne({ _id: params._id });
    }
}
