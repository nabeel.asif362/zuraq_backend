import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { TopicService } from './topic.service';
import { CreateTopicDto, DeleteTopicDto } from './dto/index.dto';

@Controller('topic')
export class TopicController {
    constructor(private readonly topicService: TopicService) {}

    @Post()
    create(@Body() body: CreateTopicDto) {
        return this.topicService.create(body);
    }

    @Get()
    findAll() {
        return this.topicService.findAll();
    }

    @Delete(':_id')
    remove(@Param() params: DeleteTopicDto) {
        return this.topicService.remove(params);
    }
}
