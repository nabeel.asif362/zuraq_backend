import { Document, Schema } from 'mongoose';
import { Prop, Schema as MongooseSchema, SchemaFactory } from '@nestjs/mongoose';

@MongooseSchema({ timestamps: true })
export class Topic extends Document {
    @Prop({ required: true, unique: true })
    name: string;
}

export const TopicSchema: Schema<Topic> = SchemaFactory.createForClass(Topic);
