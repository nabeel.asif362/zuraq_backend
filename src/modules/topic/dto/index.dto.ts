import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class CreateTopicDto {
    @IsNotEmpty()
    @IsString()
    name: string;
}

export class DeleteTopicDto {
    @IsNotEmpty()
    @IsMongoId()
    _id: string;
}
