import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { User } from '../../user/schemas/user.schema';

@Schema({ timestamps: true })
export class Team extends Document {
    @Prop({ required: true })
    name: string;

    @Prop({
        type: [{ memberId: { type: Types.ObjectId, ref: 'User' }, displayName: { type: String } }],
    })
    members: { memberId: Types.ObjectId; displayName: string }[];

    @Prop({ type: Types.ObjectId, ref: 'User', required: true })
    createdBy: User;
}

export const TeamSchema = SchemaFactory.createForClass(Team);
