import { BadRequestException, ForbiddenException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { EmailService } from '../email/email.service';
import { IUser } from '../user/interfaces/user.interfaces';
import { User } from '../user/schemas/user.schema';
import { UserQuiz } from '../userquiz/schemas/userquiz.schema';
import {
    CreateTeamDto,
    JoinTeamParamDto,
    LeaveTeamParamDto,
    RemoveTeamMemberParamDto,
    SendTeamInvitationBodyDto,
    UpdateTeamBodyDto,
    UpdateTeamParamDto,
} from './dto/index.dto';
import { Team } from './schemas/team.schema';

@Injectable()
export class TeamService {
    constructor(
        @InjectModel(Team.name) private readonly teamModel: Model<Team>,
        @InjectModel(UserQuiz.name) private readonly userQuizModel: Model<UserQuiz>,
        private readonly emailService: EmailService,
    ) {}

    async create(body: CreateTeamDto, user: User): Promise<void> {
        const isTeam = await this.teamModel.findOne({ name: body.name }).exec();

        if (isTeam) {
            throw new BadRequestException('Please choose a unique name.');
        }

        const displayName = user.name;

        await this.teamModel.create({
            name: body.name,
            members: [{ memberId: user._id, displayName }],
            createdBy: user._id,
        });
    }

    async findAll(user: IUser): Promise<Team[]> {
        return await this.teamModel
            .find({ members: { $nin: user._id } })
            .populate('members.memberId', '_id name email avatar', User.name)
            .select('name members.displayName createdBy')
            .populate('createdBy', '_id name email avatar', User.name)
            .exec();
    }

    async updateOne(param: UpdateTeamParamDto, body: UpdateTeamBodyDto, user: User): Promise<void> {
        const team: Team | null = await this.teamModel.findById(param.id).exec();

        if (!team) {
            throw new BadRequestException('Invalid team id.');
        }

        const isSameTeamName: Team | null = await this.teamModel.findOne({ name: body.name }).exec();

        if (isSameTeamName) {
            throw new BadRequestException('Please choose a unique name.');
        }

        if (team.createdBy._id.toString() !== user._id.toString()) {
            throw new ForbiddenException('Only owner can change the team name.');
        }

        await this.teamModel.findByIdAndUpdate(param.id, body);
    }

    async findMyTeams(user: IUser): Promise<Team[]> {
        return await this.teamModel
            .find({ 'members.memberId': user._id })
            .populate('members.memberId', '_id name email avatar', User.name)
            .select('name members.displayName createdBy createdAt')
            .populate('createdBy', '_id name email avatar', User.name)
            .exec();
    }

    async findMyTeamAnalytics(user: IUser): Promise<any> {
        try {
            const teams: any[] = await this.teamModel
                .find({
                    $or: [{ createdBy: user._id }, { members: user._id }],
                })
                .select('members')
                .lean();

            if (!teams || teams.length === 0) {
                return [];
            }

            const allMembers = Array.from(
                new Set(teams.flatMap((team) => team.members.map((member: any) => member.memberId))),
            );

            const userQuizzes: any = await this.userQuizModel
                .find({ user: { $in: allMembers } })
                .populate({
                    path: 'quiz',
                    select: 'title topic',
                    populate: {
                        path: 'topic',
                        select: 'name',
                    },
                })
                .populate('user', 'name')
                .lean();

            if (userQuizzes.length === 0) {
                return [];
            }

            const groupedByTeam = teams.map((team) => {
                const teamMemberQuizzes = userQuizzes.filter((quiz) =>
                    team.members.map((member: any) => member.memberId.toString()).includes(quiz?.user?._id.toString()),
                );

                const groupedByUser = teamMemberQuizzes.reduce((acc, quizEntry) => {
                    const userId = quizEntry.user._id.toString();
                    const displayName =
                        team.members.find((member: any) => member.memberId.toString() === userId)?.displayName ||
                        quizEntry.user.name;

                    if (!quizEntry.quiz || !quizEntry.quiz.topic) {
                        return acc;
                    }

                    const topicId = quizEntry.quiz.topic._id.toString();

                    if (!acc[userId]) {
                        acc[userId] = {
                            user: {
                                _id: quizEntry.user._id,
                                name: displayName,
                            },
                            topics: {},
                        };
                    }

                    if (!acc[userId].topics[topicId]) {
                        acc[userId].topics[topicId] = {
                            topic: {
                                _id: quizEntry.quiz.topic._id,
                                title: quizEntry.quiz.topic.name,
                            },
                            totalScore: 0,
                        };
                    }

                    acc[userId].topics[topicId].totalScore += quizEntry.score;

                    return acc;
                }, {});

                return {
                    teamId: team._id,
                    teamAnalytics: Object.values(groupedByUser).map((user: any) => ({
                        user: user.user,
                        topics: Object.values(user.topics),
                    })),
                };
            });

            return groupedByTeam;
        } catch (error) {
            console.error('Error in findMyTeamAnalytics:', error);
            throw error;
        }
    }

    async sendTeamInvitation(body: SendTeamInvitationBodyDto, user: User): Promise<any> {
        await this.emailService.teamInvitationEmail(body.email, body.recipientName, body.teamId, user.name);
    }

    async removeTeamMember(param: RemoveTeamMemberParamDto, user: User): Promise<void> {
        const team: Team | null = await this.teamModel.findById(param.id).exec();
        if (!team) {
            throw new BadRequestException('Invalid team id.');
        }
        if (team.createdBy._id.toString() !== user._id.toString()) {
            throw new ForbiddenException('Only the team owner can perform this action.');
        }

        await this.teamModel.findByIdAndUpdate(param.id, {
            $pull: { members: { memberId: new Types.ObjectId(param.memberId) } },
        });
    }

    async joinTeam(param: JoinTeamParamDto, user: User): Promise<void> {
        const team = await this.teamModel.findById(param.id).exec();
        if (!team) {
            throw new BadRequestException('Team not found.');
        }

        const isMember = team.members.some((memberId) => memberId.toString() === user._id.toString());
        if (isMember) {
            throw new BadRequestException('You are already a member of this team.');
        }

        await this.teamModel.findByIdAndUpdate(param.id, {
            $push: { members: { memberId: user._id, displayName: user.name } },
        });
    }

    async leaveTeam(param: LeaveTeamParamDto, user: User): Promise<void> {
        const team = await this.teamModel.findById(param.id).populate('createdBy').exec();

        if (!team) {
            throw new BadRequestException('The team you are trying to leave does not exist.');
        }

        const isMember = team.members.some((memberId) => memberId.toString() === user._id.toString());
        if (!isMember) {
            throw new BadRequestException('You need to be a part of the team before leaving it.');
        }

        if (team.createdBy._id.toString() === user._id.toString()) {
            await this.teamModel.findByIdAndDelete(team._id);
        } else {
            await this.teamModel.findByIdAndUpdate(param.id, {
                $pull: { members: user._id },
            });
        }
    }

    async updateDisplayName(teamId: string, displayName: string, userId: Types.ObjectId): Promise<void> {
        const team = await this.teamModel.findById(teamId).exec();

        if (!team) {
            throw new BadRequestException('Team not found.');
        }

        const memberIndex = team.members.findIndex((member) => member.memberId.toString() === userId.toString());
        if (memberIndex === -1) {
            throw new BadRequestException('User is not a member of this team.');
        }

        team.members[memberIndex].displayName = displayName;
        await team.save();
    }
}
