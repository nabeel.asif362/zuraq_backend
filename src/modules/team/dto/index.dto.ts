import { IsEmail, IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class CreateTeamDto {
    @IsNotEmpty()
    @IsString()
    name: string;
}

export class JoinTeamParamDto {
    @IsNotEmpty()
    @IsMongoId()
    id: string;
}

export class SendTeamInvitationBodyDto {
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @IsString()
    recipientName: string;

    @IsNotEmpty()
    @IsString()
    teamId: string;
}

export class UpdateDisplayNameDto {
    @IsNotEmpty()
    @IsString()
    displayName: string;
}

export class LeaveTeamParamDto extends JoinTeamParamDto {}

export class UpdateTeamParamDto extends JoinTeamParamDto {}

export class UpdateTeamBodyDto extends CreateTeamDto {}

export class RemoveTeamMemberParamDto extends JoinTeamParamDto {
    @IsNotEmpty()
    @IsString()
    memberId: string;
}
