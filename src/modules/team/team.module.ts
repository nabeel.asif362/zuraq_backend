import { Module } from '@nestjs/common';
import { TeamService } from './team.service';
import { TeamController } from './team.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Team, TeamSchema } from './schemas/team.schema';
import { JwtService } from '@nestjs/jwt';
import { User, UserSchema } from '../user/schemas/user.schema';
import { UserQuiz, UserQuizSchema } from '../userquiz/schemas/userquiz.schema';
import { EmailService } from '../email/email.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: Team.name,
                schema: TeamSchema,
            },
            {
                name: User.name,
                schema: UserSchema,
            },
            {
                name: UserQuiz.name,
                schema: UserQuizSchema,
            },
        ]),
    ],
    controllers: [TeamController],
    providers: [TeamService, JwtService, EmailService],
})
export class TeamModule {}
