import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { TeamService } from './team.service';
import {
    CreateTeamDto,
    JoinTeamParamDto,
    LeaveTeamParamDto,
    SendTeamInvitationBodyDto,
    UpdateTeamParamDto,
    UpdateTeamBodyDto,
    RemoveTeamMemberParamDto,
    UpdateDisplayNameDto,
} from './dto/index.dto';
import { AuthGuard } from '../../guards/auth.guard';
import { User } from '../user/schemas/user.schema';
import { UserDecorator } from '../../decorators/user.decorator';
import { Team } from './schemas/team.schema';
import { IUser } from '../user/interfaces/user.interfaces';

@UseGuards(AuthGuard)
@Controller('team')
export class TeamController {
    constructor(private readonly teamService: TeamService) {}

    @Post()
    create(@Body() body: CreateTeamDto, @UserDecorator() user: User): Promise<void> {
        return this.teamService.create(body, user);
    }

    @Get()
    findAll(@UserDecorator() user: IUser): Promise<Team[]> {
        return this.teamService.findAll(user);
    }

    @Get('my')
    findMyTeams(@UserDecorator() user: IUser): Promise<Team[]> {
        return this.teamService.findMyTeams(user);
    }

    @Get('my/analytics')
    findMyTeamAnalytics(@UserDecorator() user: IUser): Promise<Team> {
        return this.teamService.findMyTeamAnalytics(user);
    }

    @Post('invite')
    sendTeamInvitation(@Body() body: SendTeamInvitationBodyDto, @UserDecorator() user: User): Promise<void> {
        return this.teamService.sendTeamInvitation(body, user);
    }

    @Patch(':id')
    updateOne(
        @Param() param: UpdateTeamParamDto,
        @Body() body: UpdateTeamBodyDto,
        @UserDecorator() user: User,
    ): Promise<void> {
        return this.teamService.updateOne(param, body, user);
    }

    @Delete(':id/:memberId')
    removeTeamMember(@Param() param: RemoveTeamMemberParamDto, @UserDecorator() user: User): Promise<void> {
        return this.teamService.removeTeamMember(param, user);
    }

    @Patch(':id/join')
    joinTeam(@Param() param: JoinTeamParamDto, @UserDecorator() user: User): Promise<void> {
        return this.teamService.joinTeam(param, user);
    }

    @Patch(':id/leave')
    leaveTeam(@Param() param: LeaveTeamParamDto, @UserDecorator() user: User): Promise<void> {
        return this.teamService.leaveTeam(param, user);
    }

    @Patch(':id/update-display-name')
    updateDisplayName(
        @Param('id') teamId: string,
        @Body() body: UpdateDisplayNameDto,
        @UserDecorator() user: IUser,
    ): Promise<void> {
        return this.teamService.updateDisplayName(teamId, body.displayName, user._id);
    }
}
