import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class EmailService {
    private readonly emailFrom = 'MyExpertPAL <no-reply@myexpertpal.com>';

    constructor(private readonly mailerService: MailerService) {}

    async welcomeEmail(userName: string, emailTo: string): Promise<void> {
        const subject = 'Welcome to MyExpertPal! Your Journey to Mastery Starts Here.';
        const platformUrl = 'https://myexpertpal.com/';
        const logoUrl = 'https://res.cloudinary.com/x-nabeel-x/image/upload/v1730117755/zuraq/szdqp6bk77jsngjpwzdw.png';

        const emailHtml = `
    <div style="font-family: Arial, sans-serif; color: #333; line-height: 1.6; background-color: #f8f9fc; padding: 40px;">
        <style>
            @media only screen and (max-width: 600px) {
                .email-container { width: 100% !important; padding: 20px !important; }
                .email-header, .email-footer { padding: 15px !important; font-size: 1.2em !important; }
                .email-content { padding: 20px !important; }
                .email-button { padding: 12px 24px !important; font-size: 1em !important; }
                .email-text { font-size: 1em !important; }
            }
        </style>
        <div class="email-container" style="max-width: 600px; margin: auto; background-color: #ffffff; border-radius: 8px; overflow: hidden; box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.1);">
            <div class="email-header" style="background-color: #3498db; padding: 20px; color: white; text-align: center; font-size: 1.5em; font-weight: bold;">
                <a href="${platformUrl}" style="display: inline-block; background-color: white; padding: 10px; border-radius: 8px;">
                    <img src="${logoUrl}" alt="MyExpertPal Logo" style="max-width: 120px; height: auto;">
                </a>
            </div>
            <div class="email-content" style="padding: 30px;">
                <p style="font-size: 1.2em; color: #555;">Hello ${userName},</p>
                <p>Welcome to MyExpertPal! We’re thrilled to have you on board. You’re now part of a dynamic community focused on learning, growing, and mastering topics like finance, technology, and more.</p>
                <p><strong>Here’s what you can do next:</strong></p>
                <ul style="padding-left: 20px;">
                    <li><strong>Explore Quizzes:</strong> Test your knowledge with our wide variety of quizzes.</li>
                    <li><strong>Create a Team:</strong> Invite others to join your journey and track your progress together.</li>
                    <li><strong>Become a Creator:</strong> Create your own quizzes and share your expertise with the community.</li>
                </ul>
                <p style="text-align: center; font-size: 1.1em; color: #555; margin-top: 20px;">
                    Ready to dive in? Click the link below to get started!
                </p>
                <div style="text-align: center; margin: 30px 0;">
                    <a href="${platformUrl}" 
                       class="email-button"
                       style="display: inline-block; padding: 15px 30px; color: #ffffff; background-color: #3498db; border-radius: 5px; text-decoration: none; font-weight: bold; font-size: 1.2em;">
                        Get Started
                    </a>
                </div>
                <p>Happy learning,<br>The MyExpertPal Team</p>
            </div>
            <div class="email-footer" style="background-color: #f0f0f0; padding: 15px; font-size: 0.9em; color: #777; text-align: center;">
                This email was generated automatically. Please do not reply to this email.
            </div>
        </div>
    </div>
    `;

        await this.mailerService.sendMail({
            from: this.emailFrom,
            to: emailTo,
            subject,
            html: emailHtml,
        });
    }

    async quizInvitationEmail(
        quizId: string,
        emailTo: string,
        recipientName: string,
        senderName: string,
        quizTopicName: string,
    ): Promise<void> {
        const subject = 'Challenge Yourself with a New Quiz on MyExpertPal!';
        const quizPageUrl = `https://myexpertpal.com/user/quiz/${quizId}?quizShareId=${quizId}`;

        const emailHtml = `
        <div style="font-family: Arial, sans-serif; color: #333; line-height: 1.6; background-color: #f8f9fc; padding: 40px;">
            <style>
                /* Mobile Responsiveness */
                @media only screen and (max-width: 600px) {
                    .email-container {
                        width: 100% !important;
                        padding: 20px !important;
                    }
                    .email-header, .email-footer {
                        padding: 15px !important;
                        font-size: 1.2em !important;
                    }
                    .email-content {
                        padding: 20px !important;
                    }
                    .email-button {
                        padding: 12px 24px !important;
                        font-size: 1em !important;
                    }
                    .email-text {
                        font-size: 1em !important;
                    }
                }
            </style>

            <div class="email-container" style="max-width: 600px; margin: auto; background-color: #ffffff; border-radius: 8px; overflow: hidden; box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.1);">
                <!-- Header -->
                <div class="email-header" style="background-color: #3498db; padding: 20px; color: white; text-align: center; font-size: 1.5em; font-weight: bold;">
                    MyExpertPal Quiz Invitation
                </div>
                
                <!-- Body -->
                <div class="email-content" style="padding: 30px;">
                    <p class="email-text" style="font-size: 1.2em; color: #555;">Hello ${recipientName},</p>
                    <p class="email-text" style="color: #333;">I just created an exciting new quiz on MyExpertPal that I think you’ll enjoy! It’s a great opportunity to test and reinforce your knowledge on <strong>${quizTopicName}</strong>.</p>
                    <p class="email-text" style="color: #333;">Ready to take the challenge? Click the link below to attempt the quiz and see how you measure up!</p>

                    <div style="text-align: center; margin: 30px 0;">
                        <a href="${quizPageUrl}" 
                           class="email-button"
                           style="display: inline-block; padding: 15px 30px; color: #ffffff; background-color: #3498db; border-radius: 5px; text-decoration: none; font-weight: bold; font-size: 1.2em;">
                            Start Quiz
                        </a>
                    </div>

                    <p class="email-text" style="color: #333;">Let’s see who comes out on top! Feel free to share your results with the team and invite others to join the fun.</p>

                    <p class="email-text" style="margin-top: 20px; color: #333;">Best regards,<br>
                    <strong>${senderName}</strong><br>
                    MyExpertPal Team</p>
                </div>

                <!-- Footer -->
                <div class="email-footer" style="background-color: #f0f0f0; padding: 15px; font-size: 0.9em; color: #777; text-align: center;">
                    This email was generated automatically. Please do not reply to this email.
                </div>
            </div>
        </div>
        `;

        await this.mailerService.sendMail({
            from: this.emailFrom,
            to: emailTo,
            subject,
            html: emailHtml,
        });
    }

    async teamInvitationEmail(
        emailTo: string,
        recipientName: string,
        teamId: string,
        inviterName: string,
    ): Promise<void> {
        const platformUrl = 'https://myexpertpal.com/';
        const logoUrl = 'https://res.cloudinary.com/x-nabeel-x/image/upload/v1730117755/zuraq/szdqp6bk77jsngjpwzdw.png';
        const subject = `${inviterName} Has Invited You to Join Their Team on MyExpertPal!`;
        const teamJoinUrl = `https://myexpertpal.com/user/login?teamId=${teamId}`;

        const emailHtml = `
        <div style="font-family: Arial, sans-serif; color: #333; line-height: 1.6; background-color: #f8f9fc; padding: 40px;">
            <style>
                @media only screen and (max-width: 600px) {
                    .email-container { width: 100% !important; padding: 20px !important; }
                    .email-header, .email-footer { padding: 15px !important; font-size: 1.2em !important; }
                    .email-content { padding: 20px !important; }
                    .email-button { padding: 12px 24px !important; font-size: 1em !important; }
                    .email-text { font-size: 1em !important; }
                }
            </style>
            <div class="email-container" style="max-width: 600px; margin: auto; background-color: #ffffff; border-radius: 8px; overflow: hidden; box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.1);">
                <div class="email-header" style="background-color: #3498db; padding: 20px; color: white; text-align: center; font-size: 1.5em; font-weight: bold;">
                    <a href="${platformUrl}" style="display: inline-block; background-color: white; padding: 10px; border-radius: 8px;">
                        <img src="${logoUrl}" alt="MyExpertPal Logo" style="max-width: 120px; height: auto;">
                    </a>
                </div>
                <div class="email-content" style="padding: 30px;">
                    <p style="font-size: 1.2em; color: #555;">Hello ${recipientName},</p>
                    <p>${inviterName} has invited you to join their team on MyExpertPal! As part of the team, you can take on exciting quizzes together, share results, and inspire each other to learn and grow.</p>
                    <div style="text-align: center; margin: 30px 0;">
                        <a href="${teamJoinUrl}" 
                           class="email-button"
                           style="display: inline-block; padding: 15px 30px; color: #ffffff; background-color: #3498db; border-radius: 5px; text-decoration: none; font-weight: bold; font-size: 1.2em;">
                            Join Team
                        </a>
                    </div>
                    <p>Looking forward to seeing you on the team,<br>The MyExpertPal Team</p>
                </div>
                <div class="email-footer" style="background-color: #f0f0f0; padding: 15px; font-size: 0.9em; color: #777; text-align: center;">
                    This email was generated automatically. Please do not reply to this email.
                </div>
            </div>
        </div>
        `;

        await this.mailerService.sendMail({
            from: this.emailFrom,
            to: emailTo,
            subject,
            html: emailHtml,
        });
    }

    async forgotPasswordEmail(emailTo: string, userName: string, resetToken: string): Promise<void> {
        const subject = 'Reset Your Password on MyExpertPal';
        const platformUrl = 'https://myexpertpal.com';
        const logoUrl = 'https://res.cloudinary.com/x-nabeel-x/image/upload/v1730117755/zuraq/szdqp6bk77jsngjpwzdw.png';
        const resetUrl = `${platformUrl}/reset-password/${resetToken}`;

        const emailHtml = `
    <div style="font-family: Arial, sans-serif; color: #333; line-height: 1.6; background-color: #f8f9fc; padding: 40px;">
        <style>
            @media only screen and (max-width: 600px) {
                .email-container { width: 100% !important; padding: 20px !important; }
                .email-header, .email-footer { padding: 15px !important; font-size: 1.2em !important; }
                .email-content { padding: 20px !important; }
                .email-button { padding: 12px 24px !important; font-size: 1em !important; }
                .email-text { font-size: 1em !important; }
            }
        </style>
        <div class="email-container" style="max-width: 600px; margin: auto; background-color: #ffffff; border-radius: 8px; overflow: hidden; box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.1);">
            <div class="email-header" style="background-color: #3498db; padding: 20px; color: white; text-align: center; font-size: 1.5em; font-weight: bold;">
                <a href="${platformUrl}" style="display: inline-block; background-color: white; padding: 10px; border-radius: 8px;">
                    <img src="${logoUrl}" alt="MyExpertPal Logo" style="max-width: 120px; height: auto;">
                </a>
            </div>
            <div class="email-content" style="padding: 30px;">
                <p style="font-size: 1.2em; color: #555;">Hello ${userName},</p>
                <p>It looks like you requested a password reset for your account. Click the button below to reset your password:</p>
                <div style="text-align: center; margin: 30px 0;">
                    <a href="${resetUrl}" 
                       class="email-button"
                       style="display: inline-block; padding: 15px 30px; color: #ffffff; background-color: #3498db; border-radius: 5px; text-decoration: none; font-weight: bold; font-size: 1.2em;">
                        Reset Password
                    </a>
                </div>
                <p>If you did not request a password reset, please ignore this email or contact support if you have any questions.</p>
                <p>Thanks,<br>The MyExpertPal Team</p>
            </div>
            <div class="email-footer" style="background-color: #f0f0f0; padding: 15px; font-size: 0.9em; color: #777; text-align: center;">
                This email was generated automatically. Please do not reply to this email.
            </div>
        </div>
    </div>
    `;

        await this.mailerService.sendMail({
            from: this.emailFrom,
            to: emailTo,
            subject,
            html: emailHtml,
        });
    }

    async resetPasswordConfirmationEmail(emailTo: string, userName: string): Promise<void> {
        const subject = 'Your Password Has Been Successfully Reset';
        const platformUrl = 'https://myexpertpal.com/';
        const logoUrl = 'https://res.cloudinary.com/x-nabeel-x/image/upload/v1730117755/zuraq/szdqp6bk77jsngjpwzdw.png';

        const emailHtml = `
    <div style="font-family: Arial, sans-serif; color: #333; line-height: 1.6; background-color: #f8f9fc; padding: 40px;">
        <style>
            @media only screen and (max-width: 600px) {
                .email-container { width: 100% !important; padding: 20px !important; }
                .email-header, .email-footer { padding: 15px !important; font-size: 1.2em !important; }
                .email-content { padding: 20px !important; }
                .email-button { padding: 12px 24px !important; font-size: 1em !important; }
                .email-text { font-size: 1em !important; }
            }
        </style>
        <div class="email-container" style="max-width: 600px; margin: auto; background-color: #ffffff; border-radius: 8px; overflow: hidden; box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.1);">
            <div class="email-header" style="background-color: #3498db; padding: 20px; color: white; text-align: center; font-size: 1.5em; font-weight: bold;">
                <a href="${platformUrl}" style="display: inline-block; background-color: white; padding: 10px; border-radius: 8px;">
                    <img src="${logoUrl}" alt="MyExpertPal Logo" style="max-width: 120px; height: auto;">
                </a>
            </div>
            <div class="email-content" style="padding: 30px;">
                <p style="font-size: 1.2em; color: #555;">Hello ${userName},</p>
                <p>Your password has been successfully reset. You can now log in to MyExpertPal with your new password.</p>
                <p>If you did not request this change, please contact our support team immediately.</p>
                <p>Thanks,<br>The MyExpertPal Team</p>
            </div>
            <div class="email-footer" style="background-color: #f0f0f0; padding: 15px; font-size: 0.9em; color: #777; text-align: center;">
                This email was generated automatically. Please do not reply to this email.
            </div>
        </div>
    </div>
    `;

        await this.mailerService.sendMail({
            from: this.emailFrom,
            to: emailTo,
            subject,
            html: emailHtml,
        });
    }
}
