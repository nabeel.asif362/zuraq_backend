import { ConfigService } from '@nestjs/config';
import { Global, Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { EmailService } from './email.service';

@Global()
@Module({
    imports: [
        MailerModule.forRootAsync({
            imports: undefined,
            useFactory: async (config: ConfigService) => ({
                transport: {
                    host: config.get('MAIL_HOST'),
                    auth: {
                        user: config.get('SMTP_USERNAME'),
                        pass: config.get('SMTP_PASSWORD'),
                    },
                    tls: {
                        rejectUnauthorized: false,
                    },
                },
            }),
            inject: [ConfigService],
        }),
    ],
    providers: [EmailService],
    exports: [EmailService],
})
export class EmailModule {}
