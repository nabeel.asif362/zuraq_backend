import { Type } from 'class-transformer';
import { IsArray, IsMongoId, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Types } from 'mongoose';

export class CreateAnswerDto {
    @IsNotEmpty()
    @IsMongoId()
    questionId: Types.ObjectId;

    @IsNotEmpty()
    @IsMongoId()
    selectedOptionId: Types.ObjectId;
}

export class CreateUserQuizDto {
    @IsNotEmpty()
    @IsMongoId()
    quiz: Types.ObjectId;

    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => CreateAnswerDto)
    answers: CreateAnswerDto[];
}

export class CreateQuizForUnverifiedUserParamDto {
    @IsNotEmpty()
    @IsString()
    guestId: string;
}
