import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateQuizForUnverifiedUserParamDto, CreateUserQuizDto } from './dto/userquiz.dto';
import { User } from '../user/schemas/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { UserQuiz } from './schemas/userquiz.schema';
import { Quiz } from '../quiz/schemas/quiz.schema';

@Injectable()
export class UserquizService {
    constructor(
        @InjectModel(UserQuiz.name) private readonly userQuizModel: Model<UserQuiz>,
        @InjectModel(Quiz.name) private readonly quizModel: Model<Quiz>,
    ) {}

    async create(body: CreateUserQuizDto, user: User) {
        const quiz: Quiz = await this.quizModel.findById(body.quiz).exec();

        const answers = body.answers.map((answer) => {
            const questionId = new Types.ObjectId(answer.questionId);
            const selectedOptionId = new Types.ObjectId(answer.selectedOptionId);

            const question = quiz.questions.find((q: any) => q._id.equals(questionId));
            if (!question) {
                throw new NotFoundException(`Question with ID ${answer.questionId} not found in quiz ${quiz._id}`);
            }

            const selectedOption = question.options.find((o: any) => o._id.equals(selectedOptionId));
            if (!selectedOption) {
                throw new NotFoundException(
                    `Option with ID ${answer.selectedOptionId} not found in question ${question._id}`,
                );
            }

            return {
                questionId: answer.questionId,
                selectedOptionId: answer.selectedOptionId,
                isCorrect: selectedOption.isCorrect,
            };
        });

        const score = answers.filter((answer) => answer.isCorrect).length;

        await this.userQuizModel.create({
            user: user._id,
            quiz: body.quiz,
            answers: answers,
            score: score,
            completedAt: Date.now(),
        });

        const scorePercentage: number = Number(((score / answers.length) * 100).toFixed(2));

        return scorePercentage >= 80
            ? { scorePercentage, status: 'PASS', message: `Congratulations!, You have passed ${quiz.title}` }
            : { scorePercentage, status: 'FAIL', message: `Unfortunately, You have failed ${quiz.title}` };
    }

    async createForUnverifiedUser(query: CreateQuizForUnverifiedUserParamDto, body: CreateUserQuizDto) {
        const hasAlreadyAttemptQuiz: UserQuiz | null = await this.userQuizModel
            .findOne({ guestId: query.guestId })
            .exec();

        if (hasAlreadyAttemptQuiz)
            throw new BadRequestException(
                'Great to see you back! To attempt another quiz, please create an account for a better experience!',
            );

        const quiz: Quiz = await this.quizModel.findById(body.quiz).exec();

        const answers = body.answers.map((answer) => {
            const questionId = new Types.ObjectId(answer.questionId);
            const selectedOptionId = new Types.ObjectId(answer.selectedOptionId);

            const question = quiz.questions.find((q: any) => q._id.equals(questionId));
            if (!question) {
                throw new NotFoundException(`Question with ID ${answer.questionId} not found in quiz ${quiz._id}`);
            }

            const selectedOption = question.options.find((o: any) => o._id.equals(selectedOptionId));
            if (!selectedOption) {
                throw new NotFoundException(
                    `Option with ID ${answer.selectedOptionId} not found in question ${question._id}`,
                );
            }

            return {
                questionId: answer.questionId,
                selectedOptionId: answer.selectedOptionId,
                isCorrect: selectedOption.isCorrect,
            };
        });

        const score = answers.filter((answer) => answer.isCorrect).length;

        await this.userQuizModel.create({
            quiz: body.quiz,
            answers: answers,
            score: score,
            completedAt: Date.now(),
            guestId: query.guestId,
        });

        const scorePercentage: number = Number(((score / answers.length) * 100).toFixed(2));

        return scorePercentage >= 80
            ? { scorePercentage, status: 'PASS', message: `Congratulations!, You have passed ${quiz.title}` }
            : { scorePercentage, status: 'FAIL', message: `Unfortunately, You have failed ${quiz.title}` };
    }
}
