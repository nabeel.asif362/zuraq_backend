import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

@Schema()
class Answer {
    @Prop({ required: true })
    questionId: Types.ObjectId;

    @Prop({ required: true })
    selectedOptionId: Types.ObjectId;

    @Prop({ required: true })
    isCorrect: boolean;
}

const AnswerSchema = SchemaFactory.createForClass(Answer);

@Schema()
export class UserQuiz extends Document {
    @Prop({ type: Types.ObjectId, ref: 'User' })
    user: Types.ObjectId;

    @Prop({ type: Types.ObjectId, ref: 'Quiz', required: true })
    quiz: Types.ObjectId;

    @Prop({ type: [AnswerSchema], _id: true })
    answers: Answer[];

    @Prop({ required: true })
    score: number;

    @Prop({ default: Date.now })
    completedAt: Date;

    @Prop({ default: null })
    guestId: string;
}

export const UserQuizSchema = SchemaFactory.createForClass(UserQuiz);
