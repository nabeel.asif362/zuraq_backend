import { Body, Controller, Post, Query, UseGuards } from '@nestjs/common';
import { UserquizService } from './userquiz.service';
import { CreateQuizForUnverifiedUserParamDto, CreateUserQuizDto } from './dto/userquiz.dto';
import { AuthGuard } from '../../guards/auth.guard';
import { UserDecorator } from '../../decorators/user.decorator';
import { User } from '../user/schemas/user.schema';

@Controller('userquiz')
export class UserquizController {
    constructor(private readonly userquizService: UserquizService) {}

    @Post()
    @UseGuards(AuthGuard)
    create(@Body() body: CreateUserQuizDto, @UserDecorator() user: User) {
        return this.userquizService.create(body, user);
    }

    @Post('unverified')
    createForUnverifiedUser(@Query() query: CreateQuizForUnverifiedUserParamDto, @Body() body: CreateUserQuizDto) {
        return this.userquizService.createForUnverifiedUser(query, body);
    }
}
