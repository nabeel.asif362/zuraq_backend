import { Module } from '@nestjs/common';
import { UserquizService } from './userquiz.service';
import { UserquizController } from './userquiz.controller';
import { JwtService } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../user/schemas/user.schema';
import { Quiz, QuizSchema } from '../quiz/schemas/quiz.schema';
import { UserQuiz, UserQuizSchema } from './schemas/userquiz.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: User.name, schema: UserSchema },
            { name: Quiz.name, schema: QuizSchema },
            { name: UserQuiz.name, schema: UserQuizSchema },
        ]),
    ],
    controllers: [UserquizController],
    providers: [UserquizService, JwtService],
})
export class UserquizModule {}
