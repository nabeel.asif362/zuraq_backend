import { Types } from 'mongoose';

export interface IAnswer {
    questionId: Types.ObjectId;
    selectedOptionId: Types.ObjectId;
    isCorrect: boolean;
}

export interface IUserQuiz {
    user: Types.ObjectId;
    quiz: Types.ObjectId;
    answers: IAnswer[];
    score: number;
    completedAt: Date;
}
