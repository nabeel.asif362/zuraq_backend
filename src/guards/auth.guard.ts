import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Model } from 'mongoose';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../modules/user/schemas/user.schema';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        @InjectModel(User.name)
        private readonly userModel: Model<User>,
        private configService: ConfigService,
        private readonly jwtService: JwtService,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const token = this.extractTokenFromHeader(request);

        if (!token) throw new UnauthorizedException();

        try {
            const decoded = await this.jwtService.verifyAsync(token, {
                secret: this.configService.get('JWT_SECRET'),
            });

            const user: User = await this.userModel.findById(decoded.id).exec();

            if (!user) throw new UnauthorizedException();

            request['user'] = user;
        } catch (err) {
            throw new UnauthorizedException();
        }

        return true;
    }

    generateToken(id: string): string {
        const payload = { id };
        return this.jwtService.sign(payload, {
            secret: this.configService.get<string>('JWT_SECRET'),
            expiresIn: this.configService.get<string>('JWT_EXPIRE'),
        });
    }

    private extractTokenFromHeader(request: Request): string | undefined {
        const [type, token] = request.headers.authorization?.split(' ') ?? [];
        return type === 'Bearer' ? token : undefined;
    }
}
